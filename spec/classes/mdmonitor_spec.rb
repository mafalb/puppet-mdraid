#-
# Copyright (c) 2012 Markus Falb <markus.falb@fasel.at>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUcat SED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require 'spec_helper'

describe 'mdraid::mdmonitor' do

  it { should include_class('mdraid') }
  it { should include_class('mdraid::mdmonitor') }
  it { should contain_service('mdmonitor').with_enable('true') }

  context 'active mdraid' do

    let (:facts) {
      { :mdraid_count => 1 }
    }

    it { should contain_file('mdadm.conf').without_ensure }

    # One of :hasstatus or :status has to be present.
    it { should contain_service('mdmonitor').with(
      :ensure => 'running',
      :enable => true,
      :hasstatus => true )
    }
  end

  context 'no active mdraid' do
    
    let (:facts) {
      { :mdraid_count => 0 }
    }

    it { should contain_service('mdmonitor').with(
      :ensure => 'stopped',
      :enable => false,
      :hasstatus => true )
    }

  end
end
