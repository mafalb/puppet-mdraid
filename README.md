# puppet mdraid module

This module is meant to handle stuff related to the linux md raid.  
It handles [mdadm][1] and the mdmonitor service which is basically mdadm run in daemon mode.

The web page for contributing is at [https://bitbucket.org/mafalb/puppet-mdraid][2]  
The puppet forge link is [http://forge.puppetlabs.com/mafalb/mdraid][3]

## Requirements

It uses hiera, so hiera must be installed on your puppet master or on any machines you want to run the tests on.

## Usage
    
    # Start the mdmonitor service
    # This includes also the mdraid class
    include mdraid::mdmonitor

Note that it should be safe to include this even if you have no raid arrays. The mdmonitor class is clever enough to detect if you are using the md driver. There is a custom fact that returns the number of raid arrays.

    # Get the number of md raid arrays
    $count = $mdraid_count

[1]: http://neil.brown.name/blog/mdadm
[2]: https://bitbucket.org/mafalb/puppet-mdraid
[3]: http://forge.puppetlabs.com/mafalb/mdraid